# Theorie

### Definitions:

**Class**:   			*A class in C++ is a user defined type or data structure declared with keyword class that has data and functions (also called methods) as its members.*  
**Method**:     		*A member function of a class.*  
**Field**:  			*A data member of a class*  
**Constructor**:    	*A constructor is a special type of member function that initialises an object automatically when it is created.*  
**Object**:     		*An instance of a Class.*  

### Example:

Class: 		std::string  
Method: 	size()  
Constructor:	http://www.cplusplus.com/reference/string/string/string/  
Object:		std::string text;  



### Code example


~~~~
#include <iostream>

// class definition
class Car {
public:
    // constructor declaration
    Car(int horsepow);
    // class method declaration
    void printValue();
    // data member
    int horsePower = 0;
};

// body of the constructor (definition)
// <class>::<function>(<agruments>) { body }
Car::Car(int horsepow) {
    std::cout << "horsepower: " << horsePower << std::endl;
    horsePower = horsepow;
    std::cout << "horsepower: " << horsePower << std::endl;
    std::cout << "inside constructor" << std::endl;
}

// Car member function definition
void Car::printValue() {
}

int main() {
    // object of type Car
    Car carObject(500);
}
~~~~