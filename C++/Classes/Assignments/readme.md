## Assignments

#### 1. Write down 3 reasons to use classes.

#### 2. Try to think of functional additions to these classes:  
You may use pseudocode.

~~~~
class Car {
public:
    Car();
};

class Person {
public:
    Person();
};
~~~~

#### 3. Use the constructor to initiate the horsePower value via an argument

~~~~
#include <iostream>

class Car {
public:
    Car();
    void printHorsePower() {
        std::cout << horsePower << std::endl;
    }
    
private:
    int horsePower;
};

int main() {
    Car car;
    car.printHorsePower();
}
~~~~ 

#### 4. If not done already, convert all pseudocode from assignment 2 to working C++.  
####    Use prints to test your code.

